// Board.java
package oop.asg04;

/**
 * CS108 Tetris Board.
 * Represents a Tetris board -- essentially a 2-d grid
 * of booleans. Supports tetris pieces and row clearing.
 * Has an "undo" feature that allows clients to add and remove pieces efficiently.
 * Does not do any drawing or have any idea of pixels. Instead,
 * just represents the abstract 2-d board.
 */
public class Board {
    public static final int PLACE_OK = 0;
    public static final int PLACE_ROW_FILLED = 1;
    public static final int PLACE_OUT_BOUNDS = 2;
    public static final int PLACE_BAD = 3;
    boolean committed;


    // Here a few trivial methods are provided:
    // Some ivars are stubbed out for you:
    private int width;
    private int height;
    private boolean[][] grid;
    private boolean DEBUG = true;
    private int maxHeight;
    private boolean[][] backupgrid;
    public int[] widths;
    public int[] heights;


    /**
     * Creates an empty board of the given width and height
     * measured in blocks.
     */
    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        widths = new int[height];
        heights = new int[width];
        grid = new boolean[width][height];
        committed = true;
        backupgrid = new boolean[width][height];
        maxHeight=0;

        // YOUR CODE HERE
    }

    /**
     * Returns the width of the board in blocks.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the board in blocks.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the max column height present in the board.
     * For an empty board this is 0.
     */
    public int getMaxHeight() {
        return maxHeight; // YOUR CODE HERE
    }

    /**
     * Checks the board for internal consistency -- used
     * for debugging.
     */
    public void sanityCheck() {
        if (DEBUG) {
            int[] tempWidths = new int[height];
            System.arraycopy(widths, 0, tempWidths, 0, height);
            int[] tempHeights = new int[width];
            System.arraycopy(heights, 0, tempHeights, 0, width);
            int tempMaxh = maxHeight;
            setHeights();
            setWidths();
            for (int i = 0; i < height; i++) {
                if(tempWidths[i]!=widths[i]){
                    throw new RuntimeException("widths not match");
                }
            }
            for (int i = 0; i < width; i++) {
                if(tempHeights[i]!=heights[i]){
                    throw new RuntimeException("heights not match");
                }
            }
            if(tempMaxh!=maxHeight) {
                throw new RuntimeException("maxHeight not match");
            }
            // YOUR CODE HERE
        }
    }

    /**
     * Given a piece and an x, returns the y
     * value where the piece would come to rest
     * if it were dropped straight down at that x.
     * <p/>
     * <p/>
     * Implementation: use the skirt and the col heights
     * to compute this fast -- O(skirt length).
     */
    public int dropHeight(Piece piece, int x) {
        int[] pieceSkirt = piece.getSkirt();
        int y = 0;
        for (int i = 0; i < pieceSkirt.length; i++)
        {
            if(y<heights[x+i] - pieceSkirt[i]){
                y=heights[x+i] - pieceSkirt[i];
            }
        }
        return y; // YOUR CODE HERE
    }

    /**
     * Returns the height of the given column --
     * i.e. the y value of the highest block + 1.
     * The height is 0 if the column contains no blocks.
     */
    public int getColumnHeight(int x) {
        return heights[x]; // YOUR CODE HERE
    }

    /**
     * Returns the number of filled blocks in
     * the given row.
     */
    public int getRowWidth(int y) {
        return widths[y]; // YOUR CODE HERE
    }

    /**
     * Returns true if the given block is filled in the board.
     * Blocks outside of the valid width/height area
     * always return true.
     */
    public void backUp(){
        for (int i = 0; i< width; i++) {
            System.arraycopy(grid[i], 0, backupgrid[i], 0, height);
        }
    }
    public boolean getGrid(int x, int y) {
        if(grid[x][y]||x<0||y<0||x>width||y>height){
            return true;
        }
        return false; // YOUR CODE HERE
    }

    /**
     * Attempts to add the body of a piece to the board.
     * Copies the piece blocks into the board grid.
     * Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
     * for a regular placement that causes at least one row to be filled.
     * <p/>
     * <p>Error cases:
     * A placement may fail in two ways. First, if part of the piece may falls out
     * of bounds of the board, PLACE_OUT_BOUNDS is returned.
     * Or the placement may collide with existing blocks in the grid
     * in which case PLACE_BAD is returned.
     * In both error cases, the board may be left in an invalid
     * state. The client can use undo(), to recover the valid, pre-place state.
     */
    public int place(Piece piece, int x, int y) {
        // flag !committed problem
        int result = PLACE_OK;
        backUp();
        if (!committed) throw new RuntimeException("place commit problem");
        TPoint[] temp_body = piece.getBody();
        if(piece.getWidth() + x >width||piece.getHeight()+y>height||x <0 ||y<0){
            return PLACE_OUT_BOUNDS;
        }
        for (int i = 0; i < 4; i++) {
            if(grid[temp_body[i].x + x][temp_body[i].y+y]){
                return PLACE_BAD;
            }
        }

        for (int i = 0; i < 4; i++) {
            grid[temp_body[i].x + x][temp_body[i].y+y] = true;
        }
        setHeights();
        for (int i = 0; i < height; i++) {
            setWidths();
            if(widths[i]==width){
                result=PLACE_ROW_FILLED;
            }
        }
        // YOUR CODE HERE
        committed=false;
        return result;
    }
    private void setHeights(){
        for (int i = 0; i < width; i++) {
            heights[i]=0;
            for (int j = 0; j < height; j++) {
                if(grid[i][j]){
                    heights[i] = j+1;
                }
            }
        }
        maxHeight = 0;
        for (int i = 0; i < width; i++ )
        {
            if (maxHeight < heights[i]){
                maxHeight = heights[i];
            }
        }
    }
    private void setWidths(){
        for (int i = 0; i < height; i++) {
            widths[i] = 0;
            for (int j = 0; j < width; j++) {
                if (grid[j][i]) {
                    widths[i]++;
                }
            }
        }
    }

    /**
     * Deletes rows that are filled all the way across, moving
     * things above down. Returns the number of rows cleared.
     */
    public int clearRows() {
        int rowsCleared = 0;
        // YOUR CODE HERE
        for (int i = 0; i < height ; i++) {
            if(widths[i]==width){
                for (int j = 0; j < width; j++) {
                    for (int k = i; k <= maxHeight; k++) {
                        grid[j][k]=grid[j][k+1];
                    }
                }
                rowsCleared++;
                i--;
                setHeights();
                setWidths();
            }
        }
        sanityCheck();
        committed=false;
        return rowsCleared;
    }


    /**
     * Reverts the board to its state before up to one place
     * and one clearRows();
     * If the conditions for undo() are not met, such as
     * calling undo() twice in a row, then the second undo() does nothing.
     * See the overview docs.
     */
    public void undo() {
        for (int i = 0; i< width; i++) {
            System.arraycopy(backupgrid[i], 0, grid[i], 0, height);
        }

        setWidths();
        setHeights();
        committed = true;
        backUp();
        // YOUR CODE HERE
    }


    /**
     * Puts the board in the committed state.
     */
    public void commit() {
        committed = true;
    }


    /*
     Renders the board state as a big String, suitable for printing.
     This is the sort of print-obj-state utility that can help see complex
     state change over time.
     (provided debugging utility)
     */
    public String toString() {
        StringBuilder buff = new StringBuilder();
        for (int y = height - 1; y >= 0; y--) {
            buff.append('|');
            for (int x = 0; x < width; x++) {
                if (getGrid(x, y)) buff.append('+');
                else buff.append(' ');
            }
            buff.append("|\n");
        }
        for (int x = 0; x < width + 2; x++) buff.append('-');
        return (buff.toString());
    }
}